PWD = `pwd`

DEBUG =
OPTIMIZATION = -O3
FLOWTRACE =
PARALL = -fopenmp
STANDARD = -std=c++14
CFLAGS = $(DEBUG) $(OPTIMIZATION) $(FLOWTRACE) $(PARALL) $(STANDARD)
COMPILER = g++
LIBS = -lgsl -lgslcblas 
INCLUDES = -I. -Igsl/include/


S10=\
pp-ic-gen.cpp

EXE10=\
ic_gen


protonshape: 
	$(COMPILER) $(PARALL) $(S10) -o $(EXE10) $(LIBS) $(STANDARD) $(OPTIMIZATION)

clean: 
	rm -f $(OBJ10) $(OBJ11)
	rm -f $(EXE10) $(EXE11)
	rm -f results/npart/* results/ncoll/* results/sum/* results/*.dat
	rm -f *_pbs.log
