#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_sf_bessel.h>
#include <omp.h>
#include <random>
#include <sstream>
#include <gsl/gsl_errno.h>

using namespace std;


//various constant
string tab="\t";
string prefix="===> ";
double pi=M_PI;
const double fmtoGeV=5.0677;
double m_proton = 0.938272;
//double m_proton = 0.939;
double mu_proton = 2.792782;
//Physical system size [fm]
double sizeMax=4;

//run constants
int events=1000;
const int NUMT_base=50;
int *NUMT;
//number of lattices
const int lattices=1;
const double edScale=1.0;

//variables
double *q,*p,*stepsize,****rhoInteg,**rhoIntegNorm;
//overwritten in 'void evaluate()'
double B=0.0;

int seed=11111;
std::mt19937_64 rng( seed );
std::uniform_real_distribution<> impactB(0,1);


void allocateMemory()
{
  //Wasting 9 entries per array...
  p = new double[17];
  q = new double[17];

  NUMT = new int[lattices];
  for (size_t lattice=0;lattice<lattices;lattice++)
    NUMT[lattice] = NUMT_base * pow(2,lattice); //Creates lattice of multiple of NUMT_base

  for(int i=0;i<lattices;i++)
    stepsize = new double[lattices];

  cout << "Lattice points" << tab << "Step size [fm]" << tab << "AT [1/GeV]" << endl;
  for(int i=0;i<lattices;i++)
    {
      stepsize[i] = double(sizeMax/(NUMT[i]-1));
      cout << NUMT[i] << tab << tab << stepsize[i] << tab << stepsize[i]*fmtoGeV << endl;
    }
  cout << endl;
  //cout << prefix << "Lattices allocated." << endl;


  
  rhoInteg = new double***[lattices];
  for(int i=0;i<lattices;i++)
    rhoInteg[i] = new double**[2];

  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      rhoInteg[i][j] = new double*[NUMT[i]];

  for(int i=0;i<lattices;i++)
    for(int j=0;j<2;j++)
      for(int k=0;k<NUMT[i];k++)
	rhoInteg[i][j][k] = new double[NUMT[i]];

  rhoIntegNorm = new double*[lattices];

  for(int i=0;i<lattices;i++)
    rhoIntegNorm[i] = new double[2];
  
  
  cout << prefix << "Memory allocated." << endl;
}

void assignValues()
{
  p[2] = 9.70703681;
  q[2] = 14.5187212;
  p[4] = 3.7357e-4;
  q[4] = 40.88333;
  p[6] = -1.43573;
  q[6] = 2.90966;
  p[8] = 6.0e-8;
  q[8] = 99.999998;
  p[10] = 1.19052066;
  q[10] = -1.11542229;
  p[12] = 9.9527277;
  q[12] = 4.579e-5;
  p[14] = 2.5455841e-1;
  q[14] = 3.866171e-2;
  p[16] = 12.7977739;
  q[16] = 10.3580447;
  cout << prefix << "Fit values p_i and q_i assigned." << endl;
}

void latticeWarning()
{
  if (lattices!=1)
    {
      cout << "Warning: your output files are overwritten for each lattice!" << endl;
      cout << "Add additional number to each output file in the output routines!" << endl;
    }
}

double tau(double Q)
{
  return Q*Q/4.0/m_proton/m_proton;
}

double Gi(double Q, double *a, double mu)
{
  //G_E takes small q array, mu = 1
  //G_M takes small p array, mu = mu_proton
  double temp;
  temp = (1.0 + a[6] * tau(Q) + a[10] * tau(Q) * tau(Q) + a[14] * tau(Q) * tau(Q) * tau(Q))/( 1.0 + a[2] * tau(Q) + a[4] * tau(Q) * tau(Q) + a[8] * pow(tau(Q),3) + a[12]  * pow(tau(Q),4) + a[16] * pow(tau(Q),5) );
  temp*= mu;
  return temp;
}

double F1(double Q)
{
  return ( Gi(Q,q,1) + tau(Q) * Gi(Q,p,mu_proton) )/ ( 1.0 + tau(Q) );
}

double rhoIntegrand(double Q,void *params)
{
  double r = *(double *) params;
  return Q * F1(Q) * gsl_sf_bessel_J0( Q * r * fmtoGeV );
}

double rho(double *radius)
{
  double rMag, result, error;
  int precision;
  precision=10000000;


  //The transverse charge density should have only two entries x,y/r_T
  rMag = radius[0] * radius[0] + radius[1] * radius[1];
  rMag = sqrt(rMag);
  gsl_integration_workspace * w = gsl_integration_workspace_alloc (precision);
  gsl_function F;
  F.function = &rhoIntegrand;
  //passes rMag to the integration as a parameter
  F.params = &rMag;
  //gsl_integration_qags (&F,0,upperL,0,1e-7,precision,w,&result,&error);
  //Integrates from 0 to infinity; has one argument less than 'qags'
  gsl_integration_qagiu (&F,0,1e-8,1e-7,precision,w,&result,&error);
  gsl_integration_workspace_free (w);
  return fabs(result)/2.0/pi*edScale;
}

//main evaluation/calculation routine
void evaluate(int event)
{


  //This changes the impact parameter 'B' in a random fashion globally!
  B=impactB(rng);
  if(B>sizeMax/2.0)
    cout << "Warning: B is larger than half the system size" << endl;

  for (size_t lattice=0; lattice<lattices; lattice++)
    {
#pragma omp parallel for num_threads(omp_get_max_threads())
      for(int sx=0;sx<NUMT[lattice];sx++)
	for(int sy=0;sy<NUMT[lattice];sy++)
	    {
	      double r[2][2];
	      for(int nucleus=0;nucleus<2;nucleus++)
		{
		  r[nucleus][0] = -sizeMax/2.0 + sx * stepsize[lattice] + pow(-1,nucleus) * B/2.0;
		  r[nucleus][1] = -sizeMax/2.0 + sy * stepsize[lattice];
		  rhoInteg[lattice][nucleus][sx][sy] = rho(r[nucleus]);
		  if( rhoInteg[lattice][nucleus][sx][sy] < 0 )
		    {
		      cout << "Warning: density below zero." << endl;
		      cout << "B=" << B << endl;
		      cout << "Event:" << event << endl;
		    }
		}
	    }
    }
  
  //This integrates the charge density; no idea about faster or slower than MC
  for(int lattice=0;lattice<lattices;lattice++)
    for(int nucleus=0;nucleus<2;nucleus++)
      {
	//integrhoSummed(lattice, nucleus);
	double temp=0;
#pragma omp parallel for reduction (+:temp) num_threads(omp_get_max_threads())
	for(int sx=0;sx<NUMT[lattice];sx++)
	  for(int sy=0;sy<NUMT[lattice];sy++)
	    temp+= rhoInteg[lattice][nucleus][sx][sy];
	temp*= stepsize[lattice] * stepsize[lattice];
	rhoIntegNorm[lattice][nucleus] = temp;
	//cout << "parallel" << tab << nucleus <<  tab << temp << endl;

      }
}

double overlap(int sx, int sy, int lattice, double sigma)
{
  double T[2], temp;
  //converts sigma from [mb] to [fm^2]; note that 'rhoU' and 'rhoL' are in fm^{-2}
  sigma/= 10.0;
  
  //initialises the values at each lattice point
  for(int nucleus=0;nucleus<2;nucleus++)
    {
      T[nucleus]  = rhoInteg[lattice][nucleus][sx][sy]/rhoIntegNorm[lattice][nucleus];
    }

  //calculates the overlap between each nucleus, smoothed by the exp fct.s
  temp = T[0]*(1-exp(-sigma * T[1])) + T[1]*(1-exp(-sigma * T[0]));
  return temp;
}

//cross section 'sigma' in [mb]
void outputMatrix1(string output, int lattice, int event, int sigma)
{
  latticeWarning();
  ostringstream leadingZeros;
  leadingZeros << "_" <<  setw(3) << setfill('0') << event;
  output = output + leadingZeros.str() + ".dat";
  ofstream exporting (output.c_str());

  if (exporting.is_open())
    {
      for(int sx=0;sx<NUMT[lattice];sx++)
	{
	  for(int sy=0;sy<NUMT[lattice];sy++)  
	    {
	      //Output for Npart 
	      exporting << overlap(sx,sy,lattice,sigma) << tab;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << output << endl;
}

//cross section 'sigma' in [mb]
void outputMatrix2(string output, int lattice, int event, int sigma)
{
  latticeWarning();
  ostringstream leadingZeros;
  leadingZeros << "_" << setw(3) << setfill('0') << event;
  output = output + leadingZeros.str() + ".dat";
  ofstream exporting (output.c_str());
  if (exporting.is_open())
    {
      for(int sx=0;sx<NUMT[lattice];sx++)
	{
	  for(int sy=0;sy<NUMT[lattice];sy++)
	    {
	      //Output for Ncoll
	      exporting << rhoInteg[lattice][0][sx][sy]/rhoIntegNorm[lattice][0] * rhoInteg[lattice][1][sx][sy]/rhoIntegNorm[lattice][1] << tab;
	      //exporting << rhoInteg[lattice][0][sx][sy] * rhoInteg[lattice][1][sx][sy] << tab;

	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << output << endl;
}

void outputMatrix3(string output, int lattice, int event, int sigma)
{
  latticeWarning();
  ostringstream leadingZeros;
  leadingZeros << "_" <<  setw(3) << setfill('0') << event;
  output = output + leadingZeros.str() + ".dat";
  ofstream exporting (output.c_str());

  if (exporting.is_open())
    {
      for(int sx=0;sx<NUMT[lattice];sx++)
	{
	  for(int sy=0;sy<NUMT[lattice];sy++)
	    {
	      //Output for Npart 
	      exporting << rhoInteg[lattice][0][sx][sy] + rhoInteg[lattice][1][sx][sy] << tab;
	    }
	  exporting << endl;
	}
      exporting.close();
    }
  else cout << "Unable to open file " << output << endl;
}

void newoutput(string output, int lattice,int event, int sigma, double **array)
{
  ofstream exporting (output.c_str());
  if (exporting.is_open())
    { 
      for(int sx=0;sx<NUMT[lattice];sx++)
	{
	  for(int sy=0;sy<NUMT[lattice];sy++)
	    {
	    exporting << array[sx][sy] << tab;
	    }
	}
      exporting.close();
    }
 else cout << "Unable to open file " << output << endl;
}

void plotscript(string filename)
{

  ofstream exporting (filename.c_str());
  if (exporting.is_open())
    {
      exporting << "set size square" << endl;
      exporting << "set term png" << endl;
      exporting << "input1='results/npart/inited_pp_'" << endl;
      exporting << "input2='results/ncoll/inited_pp_'" << endl;
      exporting << "input3='results/sum/inited_pp_'" << endl;
      exporting << "output1='results/npart/contour_'" << endl;
      exporting << "output2='results/ncoll/contour_'" << endl;
      exporting << "output3='results/sum/contour_'" << endl;

      exporting << "#set cbrange [0:0.003]" << endl;
      for (int i=0;i<events;i++)
	{
	  exporting << "set out output1.'" << setw(3) << setfill('0') << i << ".png'" << endl;
	  exporting << "plot input1.'" << setw(3) << setfill('0') << i << ".dat' matrix with image" << endl;
	  exporting << "set out output2.'" << setw(3) << setfill('0') << i << ".png'" << endl;
	  exporting << "plot input2.'" << setw(3) << setfill('0') << i << ".dat' matrix with image" << endl;
	  exporting << "set out output3.'" << setw(3) << setfill('0') << i << ".png'" << endl;
	  exporting << "plot input3.'" << setw(3) << setfill('0') << i << ".dat' matrix with image" << endl;
	  
	}
      exporting.close();
    }
  else cout << "Unable to open file " << filename << endl;
}



void outputImpact(string filename, int event)
{
  ofstream exporting (filename.c_str(),  ios::app);
  if (exporting.is_open())
    {
      exporting << event << tab << B << endl;
      exporting.close();
    }
  else cout << "Unable to open file " << filename << endl;
}


void performOutputs()
{

  cout << prefix << "Performing outputs." << endl;
  int counter=0;

  plotscript("contours.plt");
  cout << prefix << "Plot script created." << endl;

  for(int event=0;event<events;event++)
    {
      evaluate(event);

      for(int lattice=0;lattice<lattices;lattice++)
	{
	  //#pragma omp parallel num_threads(2)
	  {
	    outputMatrix1("results/npart/inited_pp",lattice,event,70);
	    outputMatrix2("results/ncoll/inited_pp",lattice,event,70);
	    outputMatrix3("results/sum/inited_pp",lattice,event,70);
	    outputImpact("results/impact.dat",event);
	  }
	}
      counter++;
      if(double(counter%10)==0)
	cout << "Outputs written: " << tab << counter << endl;
    }
  cout << prefix << "Outputs performed." << endl;


  int dump = system("gnuplot 'contours.plt'");
  cout << prefix << "Plots created." << endl;
      
}

void preamble()
{
  cout << endl;
  cout << prefix << "This is protonshape-2.0." << endl;
  cout << endl;
  cout << "Number of cores: " << tab << omp_get_max_threads() << endl;
  cout << "Number of events: " << tab << events << endl;
  cout << "System size [fm]: " << tab << sizeMax << endl;
  cout << "Scaling: " << tab << edScale << endl;
  cout << endl;
}
  
int main()
{
  preamble();
  //Small difference to the more precise value for the proton's mass
  // m_proton=0.939;

  allocateMemory();
  assignValues();
  
  performOutputs();
  cout << prefix << "Done." << endl;
  return 0;

}
